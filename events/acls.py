from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY

import requests


def get_photo(city, state):
    pass
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_lat_lon(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # Create the URL for the geocoding API with the city and state
    params = {"q": f"{city},{state},USA", "appid": OPEN_WEATHER_API_KEY}
    res = requests.get(url, params=params)
    # Make the request
    the_json = res.json()
    # Parse the JSON response
    lat = the_json[0]["lat"]
    lon = the_json[0]["lon"]
    # Get the latitude and longitude from the response
    return lat, lon


def get_weather_data(city, state):
    lat, lon = get_lat_lon(city, state)
    url = "https://api.openweathermap.org/data/2.5/weather"
    # Create the URL for the current weather API with the latitude
    #   and longitude
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    res = requests.get(url, params=params)
    # Make the request
    the_json = res.json()
    # Parse the JSON response
    return {
        "description": the_json["weather"][0]["description"],
        "temp": the_json["main"]["temp"],
    }
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
